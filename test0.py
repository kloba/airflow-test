from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.operators.dagrun_operator import TriggerDagRunOperator
from datetime import datetime, timedelta
from airflow.models import Variable


i = datetime.now()


default_args = {
    'owner': 'alex',
    'start_date': datetime(i.year, i.month, i.day),
    'email_on_failure': True,
    'retries': 10,
    'retry_delay': timedelta(minutes=1),
    'depends_on_past': True,
}


with DAG('sample2', default_args=default_args, schedule_interval='@once') as dag:
    script = BashOperator(
        task_id='SafeSH_WholeProcess_Up_To_Real_Time',
        bash_command="""echo "hello" """
    )
